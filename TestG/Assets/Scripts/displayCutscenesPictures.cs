﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class displayCutscenesPictures : MonoBehaviour
{
    public Texture[] frames;
    double frameTime = 9.0;
    private int frame = 0;
    private double nextFrameTime = 0.0;

    void OnGUI()
    {
        if (frame < frames.Length)
        {
            
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), frames[frame]);
            if (Time.time >= nextFrameTime)
            {
                frame++;
                nextFrameTime += frameTime;
            }
        }
    }
}
