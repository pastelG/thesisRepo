﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class QuestionPart2 {

    //[TextArea (3, 10)]
    //public string question;

    public Sprite imgCode;

    [TextArea(1, 2)]
    public string ansC;

    [TextArea(1, 2)]
    public string hint;

    public bool hintUnlocked;
    
}
