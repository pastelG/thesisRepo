﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;




public class QuestionManager : MonoBehaviour {

    public GameObject verticalLayoutC, verticalLayoutTF, verticalLayoutI;
    public Text questionText, questionTF, questionI;
    public Text txtChoice1, txtChoice2, txtChoice3, txtChoice4;
    public Text txtT, txtF, txtI;
    public GameObject cAud, wAud;
    public GameObject choicesQ, trueFalseQ, imageCorrect, imageWrong, idenQuiz;
    public GameObject hintPnl, pnlConfirm, pnlHint;
    public Text hint, coins;
    public GameObject smallC, smallTF, smallI;
    public InputField inputI;
    public Image imgC, imgTF, imgI;

    private int intCoins;
    private int qIndex, q2index, q3index;
    
    private int randomIndex;
    private static List<int> list = new List<int>();
    private bool hintUnlocked;

    private coinsGamesparks coinScr;
    private int lvl;
    private string chpLvl;
    

    Vector3 originalPos;

    // Use this for initialization
    void Start () {
        intCoins = PlayerPrefs.GetInt("coins", 10);

        coinScr = GameObject.Find("GameSparksObj").GetComponent<coinsGamesparks>();

        lvl = GameObject.Find("player").GetComponent<PlayerMovement>().currentLvl;

        inputI = inputI.GetComponent<InputField>();

        chpLvl = GameObject.Find("GameSparkManager").GetComponent<GS_Leaderboard>().chLvlValue;
    }
	
	// Update is called once per frame
	void Update () {
        intCoins = PlayerPrefs.GetInt("coins", 10);
        coins.text = intCoins.ToString() + " coins";
        
    }

    public void StartDisplayIdenQuestion(QuestionPart2[] questions3)
    {
        originalPos = new Vector3(verticalLayoutI.transform.position.x, verticalLayoutI.transform.position.y, verticalLayoutI.transform.position.z);
        inputI.text = "";
        //questionI.text = "\n" + questions3[q3index].question + "\n";
        imgI.sprite = questions3[q3index].imgCode;
    }

    public void StartDisplayTFQuestion(QuestionPart2[] questions2)
    {
        originalPos = new Vector3(verticalLayoutTF.transform.position.x, verticalLayoutTF.transform.position.y, verticalLayoutTF.transform.position.z);

        //questionTF.text = "\n" + questions2[q2index].question + "\n";
        imgTF.sprite = questions2[q2index].imgCode;
    }

    public void StartDisplayChoicesQuestion(Question[] questions)
    {
        originalPos = new Vector3(verticalLayoutC.transform.position.x, verticalLayoutC.transform.position.y, verticalLayoutC.transform.position.z);
        
        string[] txts = new string[4];
        string[] strings = new string[4];
        //int[] i = new int[4];
        int count = 0;

        
        

        //int randomIndex;

        strings[0] = questions[qIndex].ansC;
        strings[1] = questions[qIndex].ansW1;
        strings[2] = questions[qIndex].ansW2;
        strings[3] = questions[qIndex].ansW3;


        /*
        for (int n = 0; n < 4; n++)    //  Populate list
        {
            list.Add(n);
        }

        //int iNum;

       
        for (int i = 0; i < 4; i++)
        {
            randomIndex = Random.Range(0, list.Count - 1);    //  Pick random element from the list
            iNum = list[randomIndex];    //  i = the number that was randomly picked
            list.RemoveAt(randomIndex);   //  Remove chosen element

            txts[iNum] = strings[i];

            //Buttons[i] = (int)Random.Range(1.0f,4.0f);
        }
        */

        //questionText.text = "\n" + questions[qIndex].question + "\n";
        imgC.sprite = questions[qIndex].imgCode;

        //this foreach converts all ints to strings
        /*
        foreach (string s in strings)
        {
            bool isNumeric = int.TryParse(s, out i[count]);
            Debug.Log("i count: " + i[count]);
            count++;
        }*/

        if (strings[0].IndexOf(' ') != -1 || strings[1].IndexOf(' ') != -1 || strings[2].IndexOf(' ') != -1 || strings[3].IndexOf(' ') != -1) {
            //if may space
            Array.Sort(strings, (x, y) => x.Length.CompareTo(y.Length));
        }
        else
        {
            Array.Sort(strings, new AlphaNumericalSort());
        }

        //by p.s.w.g
        //Array.Sort(strings, (x, y) => x.Length.CompareTo(y.Length));
        //Array.Sort(strings);


        
        

        txtChoice1.text = strings[0];
        txtChoice2.text = strings[1];
        txtChoice3.text = strings[2];
        txtChoice4.text = strings[3];

        foreach (string s in strings)
        {
            Debug.Log(s);
        }


        /*
        txtChoice1.text = txts[0];
        txtChoice2.text = txts[1];
        txtChoice3.text = txts[2];
        txtChoice4.text = txts[3];
        */


    }

    //function by Petar Ivanov
    public void NumericalSort(string[] ar)
    {
        Regex rgx = new Regex("([^0-9]*)([0-9]+)");
        Array.Sort(ar, (a, b) =>
        {
            var ma = rgx.Matches(a);
            var mb = rgx.Matches(b);
            for (int ii = 0; ii < ma.Count; ++ii)
            {
                int ret = ma[ii].Groups[1].Value.CompareTo(mb[ii].Groups[1].Value);
                if (ret != 0)
                    return ret;

                ret = int.Parse(ma[ii].Groups[2].Value) - int.Parse(mb[ii].Groups[2].Value);
                if (ret != 0)
                    return ret;
            }

            return 0;
        });
    }


    SpriteRenderer rendererS;

    IEnumerator StartW()
    {
        Debug.Log("Wrong Coroutine popped up");
        imageWrong.SetActive(true);

        rendererS = imageWrong.GetComponent<SpriteRenderer>();

        /*rendererS.color = new Color(1f, 1f, 1f, 1f);
        yield return new WaitForSeconds(2.5f);
        rendererS.color = new Color(1f, 1f, 1f, 0f);*/

        float start = Time.time;
        while (Time.time <= start + 2)
        {
            Color color = rendererS.color;
            color.a = 1f - Mathf.Clamp01((Time.time - start) / 2);
            rendererS.color = color;
            yield return new WaitForEndOfFrame();
        }

    }

    IEnumerator StartC()
    {
        imageCorrect.SetActive(true);

        rendererS = imageCorrect.GetComponent<SpriteRenderer>();

        /*
        imageCorrect.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
        yield return new WaitForSeconds(2.5f);
        imageCorrect.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0f);*/

        float start = Time.time;
        while (Time.time <= start + 2)
        {
            Color color = rendererS.color;
            color.a = 1f - Mathf.Clamp01((Time.time - start) / 2);
            rendererS.color = color;
            yield return new WaitForEndOfFrame();
        }

    }

    public void CheckIfRightChoice(Question[] questions, Text clickedT)
    {
        if (choicesQ.activeInHierarchy == true)
        {
            if (clickedT.text == questions[qIndex].ansC)
            {
                cAud.gameObject.GetComponent<AudioSource>().Play();
                FindObjectOfType<PlayerMovement>().OnClickDestroyEnemy(true);
                StartCoroutine("StartC");

            }
            else
            {
                wAud.gameObject.GetComponent<AudioSource>().Play();
                FindObjectOfType<PlayerMovement>().OnClickDestroyEnemy(false);
                StartCoroutine("StartW");
            }
            qIndex++;
            verticalLayoutC.transform.position = originalPos;
        }
        
        
        
        
    }

    public void OnClickTrueFalse(QuestionPart2[] questions2, Text clickedT)
    {
        if(trueFalseQ.activeInHierarchy == true)
        {
            if(clickedT.text == questions2[q2index].ansC)
            {
                cAud.gameObject.GetComponent<AudioSource>().Play();
                FindObjectOfType<PlayerMovement>().OnClickDestroyEnemy(true);
                StartCoroutine("StartC");
            }
            else
            {
                wAud.gameObject.GetComponent<AudioSource>().Play();
                FindObjectOfType<PlayerMovement>().OnClickDestroyEnemy(false);
                StartCoroutine("StartW");
            }
            q2index++;
            verticalLayoutTF.transform.position = originalPos;
        }
        else if (idenQuiz.activeInHierarchy == true)
        {
            if (clickedT.text == questions2[q3index].ansC)
            {
                cAud.gameObject.GetComponent<AudioSource>().Play();
                FindObjectOfType<PlayerMovement>().OnClickDestroyEnemy(true);
                StartCoroutine("StartC");

            }
            else
            {
                wAud.gameObject.GetComponent<AudioSource>().Play();
                FindObjectOfType<PlayerMovement>().OnClickDestroyEnemy(false);
                StartCoroutine("StartW");
            }
            q3index++;
            verticalLayoutI.transform.position = originalPos;
        }
    }

    public void DisplayHint(Question[] questions, QuestionPart2[] questions2, QuestionPart2[] questions3)
    {
        
        if (choicesQ.activeInHierarchy == true)
        {
            hint.text = questions[qIndex].hint;
        }
        else if (trueFalseQ.activeInHierarchy == true)
        {
            hint.text = questions2[q2index].hint;
        }
        else if (idenQuiz.activeInHierarchy == true)
        {
            hint.text = questions3[q3index].hint;
        }
        
    }

    public void CheckHint(Question[] questions, QuestionPart2[] questions2, QuestionPart2[] questions3)
    {
        
        if (choicesQ.activeInHierarchy == true)
        {
            questions[qIndex].hintUnlocked = bool.Parse(PlayerPrefs.GetString("hintA" + qIndex.ToString() + chpLvl, "false"));
            hintUnlocked = questions[qIndex].hintUnlocked;
        }
        else if (trueFalseQ.activeInHierarchy == true)
        {
            questions2[q2index].hintUnlocked = bool.Parse(PlayerPrefs.GetString("hintB" + q2index.ToString() + chpLvl, "false"));
            hintUnlocked = questions2[q2index].hintUnlocked;
        }
        else if (idenQuiz.activeInHierarchy == true)
        {
            questions3[q3index].hintUnlocked = bool.Parse(PlayerPrefs.GetString("hintC" + q3index.ToString() + chpLvl, "false"));
            hintUnlocked = questions3[q3index].hintUnlocked;
        }

        if(hintUnlocked == false)
        {
            pnlConfirm.gameObject.SetActive(true);
        }
        else
        {
            pnlHint.gameObject.SetActive(true);
            DisplayHint(questions, questions2, questions3);
        }
    }

    public void UnlockHint(Question[] questions, QuestionPart2[] questions2, QuestionPart2[] questions3)
    {
        intCoins--;
        coins.text = intCoins.ToString() + " coins";
        PlayerPrefs.SetInt("coins", intCoins);

        coinScr.SaveCoinsToGameSparks(intCoins);

        pnlHint.gameObject.SetActive(true);
        DisplayHint(questions, questions2, questions3);

        if (choicesQ.activeInHierarchy == true)
        {
            questions[qIndex].hintUnlocked = true;
            PlayerPrefs.SetString("hintA"+qIndex.ToString()+chpLvl,"true");
        }
        else if (trueFalseQ.activeInHierarchy == true)
        {
            questions2[q2index].hintUnlocked = true;
            PlayerPrefs.SetString("hintB" + q2index.ToString() + chpLvl, "true");
        }
        else if (idenQuiz.activeInHierarchy == true)
        {
            questions3[q3index].hintUnlocked = true;
            PlayerPrefs.SetString("hintC" + q3index.ToString() + chpLvl, "true");
        }
    }
}
