﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using GameSparks.Api.Requests;
using GameSparks.Core;

public class RecordPlayTime : MonoBehaviour
{
    

    private float timeNow;
    private int seconds, minutes, hours, days;


    // Start is called before the first frame update
    void Start()
    {
       

        timeNow = PlayerPrefs.GetFloat("timeNow", 0);
        seconds = (int)timeNow;
        minutes = (int)(timeNow / 60);
        hours = (int)(timeNow / 60 / 60);
        days = (int)(timeNow / 60 / 60 / 24);

        new LogEventRequest().SetEventKey("PLAY_TIME").SetEventAttribute("SECS", seconds).SetEventAttribute("MINS", minutes).SetEventAttribute("HRS", hours).SetEventAttribute("DAYS", days).Send((response) =>
        {
            if (response.HasErrors)
            {
                Debug.Log(seconds + "Save playtime Log message failed ...");
            }
            else
            {
                Debug.Log(seconds + "Save playtime Log message succeeded ..." + response);
            }
        });

        DontDestroyOnLoad(this);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	

    private void OnApplicationQuit()
    {
        
        timeNow += Time.realtimeSinceStartup;
        PlayerPrefs.SetFloat("timeNow",timeNow);
        Debug.Log("timeNow " + (int)PlayerPrefs.GetFloat("timeNow", 0));

        

    }
}
