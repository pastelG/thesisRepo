﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour
{
    public AudioMixer mixer;

    private Toggle toggleMus, toggleSound;

    // Start is called before the first frame update
    void Start()
    {
        if(PlayerPrefs.GetInt("MusicOn", 1) == 1)
        {
            mixer.SetFloat("MusicParam", 0.0f);
        }
        else
        {
            mixer.SetFloat("MusicParam", -80f);
        }
        
        if(PlayerPrefs.GetInt("SFXOn", 1) == 1)
        {
            mixer.SetFloat("SFXParam", 0.0f);
        }
        else
        {
            mixer.SetFloat("SFXParam", -80f);
        }

        DontDestroyOnLoad(this);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void toggleMusicObject(Toggle toggle)
    {
        toggleMus = toggle;
    }

    public void toggleSoundObject(Toggle toggle)
    {
        toggleSound = toggle;
    }

    public void DisplayToggleOnOff()
    {
        if (PlayerPrefs.GetInt("MusicOn", 1) == 1)
        {
            toggleMus.isOn = true;
        }
        else
        {
            toggleMus.isOn = false;
        }

        if (PlayerPrefs.GetInt("SFXOn", 1) == 1)
        {
            toggleSound.isOn = true;
        }
        else
        {
            toggleSound.isOn = false;
        }

    }

    public void musicToggle(bool toggleBool)
    {
        PlayerPrefs.SetInt("MusicOn", toggleBool ? 1 : 0);

        if (toggleBool == true)
        {
            mixer.SetFloat("MusicParam", 0.0f);
        }
        else
        {
            mixer.SetFloat("MusicParam", -80f);
        }

    }

    public void sfxToggle(bool toggleBool)
    {
        PlayerPrefs.SetInt("SFXOn", toggleBool ? 1 : 0);

        if (toggleBool == true)
        {
            mixer.SetFloat("SFXParam", 0.0f);
        }
        else
        {
            mixer.SetFloat("SFXParam", -80f);
        }

    }
}
