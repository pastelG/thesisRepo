﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSceneOnClick : MonoBehaviour {

    private int scI;
    public GameObject loadingPanel;
    public Object prefabLoad;

    private string btnName;
    private int lS;

    private void Start()
    {
        prefabLoad = Resources.Load("loadingScreenCnvas");
        loadingPanel = Instantiate(prefabLoad) as GameObject;
        loadingPanel.transform.SetAsLastSibling();
    }

    public void LoadByIndex(int sceneIndex)
    {
        Time.timeScale = 1;
        scI = sceneIndex;
        StartCoroutine(StartP());
        //SceneManager.LoadScene(sceneIndex);



    }

    public void DeleteAllPref()
    {
        PlayerPrefs.DeleteAll();
    }

    IEnumerator StartP()
    {
        Time.timeScale = 1;
        loadingPanel.SetActive(true);
        yield return new WaitForSecondsRealtime(0.5f);

        SceneManager.LoadScene(scI);

    }

    public void CurrentLvlToNext(int currentScene)
    {
        LoadByIndex(currentScene + 1);
    }

    public void ButtonNameQuitGame(string name)
    {
        btnName = name;
    }

    public void ReplayCurrentScene(int scene)
    {
        lS = scene;
    }

    public void QuitLevelGame(){
        if(btnName == "replay")
        {
            LoadByIndex(lS);
        }
        else if(btnName == "level")
        {
            LoadByIndex(1);
        }
        //menu is 0
    }
}
