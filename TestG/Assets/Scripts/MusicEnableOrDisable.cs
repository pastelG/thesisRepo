﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class MusicEnableOrDisable : MonoBehaviour
{

    private AudioManager audM;
    private Toggle mToggle, sToggle;

    private void Awake()
    {
        audM = GameObject.Find("AudioManager").GetComponent<AudioManager>();
        
    }


    void Start()
    {
        mToggle = GameObject.Find("MusicToggle").GetComponent<Toggle>();
        sToggle = GameObject.Find("SFXToggle").GetComponent<Toggle>();

        audM.toggleMusicObject(mToggle);
        audM.toggleSoundObject(sToggle);

        audM.DisplayToggleOnOff();




    }

    public void passMusicToggle(bool toggleBool)
    {
        audM.musicToggle(toggleBool);
    }

    public void passSoundToggle(bool toggleBool)
    {
        audM.sfxToggle(toggleBool);
    }
}
