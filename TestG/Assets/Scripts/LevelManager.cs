﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour {

    Button[] LevelButtons;

    public GameObject showLvl;
    private int chp;
    private int ReachLevel;

    private void Awake()
    {
        
        

        
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        
    }

    public void clickChapter()
    {
        chp = showLvl.GetComponent<ShowLevelInfo>().chpNum;

        if (chp == 1)
        {
            ReachLevel = PlayerPrefs.GetInt("ReachLevel", 1);
            Debug.Log("Chp 1 reach level is " + ReachLevel);
        }
        else if (chp == 2)
        {
            ReachLevel = PlayerPrefs.GetInt("Reach2Level", 0);
            Debug.Log("Chp 2 reach level is " + ReachLevel);
        }
        else if (chp == 3)
        {
            ReachLevel = PlayerPrefs.GetInt("Reach3Level", 0);
            Debug.Log("Chp 3 reach level is " + ReachLevel);
        }
        else if (chp == 4)
        {
            ReachLevel = PlayerPrefs.GetInt("Reach4Level", 0);
            
        }

        LevelButtons = new Button[transform.childCount];

        for (int i = 0; i < LevelButtons.Length; i++)
        {
            LevelButtons[i] = transform.GetChild(i).GetComponent<Button>();
            LevelButtons[i].GetComponentInChildren<Text>().text = (i + 1).ToString();

            if (i + 1 > ReachLevel)
            {
                LevelButtons[i].interactable = false;
            }
        }
    }

}
