﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OnOfflineMenu : MonoBehaviour
{
    public Text txtOnOff;
    public GameObject imgOnOff, alertOff;

    private int alert = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            //offline
            txtOnOff.text = "Offline";
            imgOnOff.SetActive(true);
            if (alert == 0)
            {
                alertOff.SetActive(true);
                alert++;
            }
        }
        else
        {
            //online
            txtOnOff.text = "Online";
            imgOnOff.SetActive(false);
        }
    }
}
