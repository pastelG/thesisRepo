﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseTutorial : MonoBehaviour
{
    private int reachLvl;
    public GameObject pnlTutor, t2, t3, t4;
    private GameObject btnL, btnR;
    public AudioSource bgM;
    bool pause;

    public PauseScript pauseS;

    private void Awake()
    {
        
        reachLvl = PlayerPrefs.GetInt("ReachLevel", 1);
        if(reachLvl == 1)
        {
            bgM.Pause();
            pnlTutor.SetActive(true);
            pause = true;
        }
        else
        {
            this.gameObject.SetActive(false);
        }

        btnL = GameObject.Find("leftButton");
        btnR = GameObject.Find("rightButton");
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(pause == true)
        {

            pauseS.pause = true;
            
        }
        else
        {
            pauseS.pause = false;
        }
    }

    public void ResumeTime()
    {
        pause = false;
        Time.timeScale = 1;
        this.gameObject.SetActive(false);
        pauseS.pause = false;
        bgM.Play();
    }
}
