﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayPause : MonoBehaviour
{
    //script attached to the back button of the pause panel

    public GameObject ChoiceQ, TrueFalseQ, IdenQ;
    public AudioSource bgm, fight;



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlayThisBGM()
    {
        if (ChoiceQ.activeSelf == false && TrueFalseQ.activeSelf == false && IdenQ.activeSelf == false)
        {
            bgm.UnPause();
        }
        else if (ChoiceQ.activeSelf == true || TrueFalseQ.activeSelf == true || IdenQ.activeSelf == true)
        {
            fight.UnPause();
        }
    }
    public void PauseThisMusic()
    {
        if (ChoiceQ.activeSelf == true || TrueFalseQ.activeSelf == true || IdenQ.activeSelf == true)
        {
            fight.Pause();
        }
        else if (ChoiceQ.activeSelf == false && TrueFalseQ.activeSelf == false && IdenQ.activeSelf == false)
        {
            bgm.Pause();
        }
    }
}
