﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateLBContent : MonoBehaviour
{
    public GameObject lbPanel;

    public GS_Leaderboard gs;
    public HorizontalLayoutGroup imgSc;
    // Start is called before the first frame update
    void Start()
    {
        if (lbPanel.activeSelf == true)
        {
            gs.GetLeaderboard();

        }
    }

    // Update is called once per frame
    void Update()
    {
        if (lbPanel.activeSelf == true)
        {
            imgSc.spacing = (float)0.01;
            imgSc.spacing = (float)0;
        }
    }

    public void ButtonEnterUpdateSpacing()
    {
        imgSc.spacing = (float)0.01;

    }

    public void BackUpdateSpacing()
    {
        imgSc.spacing = (float)0;

    }
}
