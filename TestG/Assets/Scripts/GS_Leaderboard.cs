﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameSparks.Core;

public class GS_Leaderboard : MonoBehaviour
{
    //private PlayerMovement pmScript;
    
    private int lvl;
    private string highScStr;
    private float highSc;
    private string time, timeValue;

    public string scoreEvent, scoreAttribute, chLvlAttribute, chLvlValue, lbName;
    public Text outputData, rankTxt, nameTxt, scoreTxt, timeTxt;
    public GameObject highScorePopup, player;
    public bool isLevelScene;
    public Text currentPlayerLB;
    public UpdateLBContent up;

    private string lbSocialN, scoreSocial;

    void Awake()
    {
        //GameSparks.Api.Messages.NewHighScoreMessage.Listener += HighScoreMessageHandler; // assign the New High Score message
        

        
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangeChLvlValuePostScore(string cLVal)
    {
        chLvlValue = cLVal;
        //PostScoreBttn();
    }

    public void ChangeNames(string lb)
    {
        lbName = lb;

        //get the partition of a leaderboard that has the event attribute value C1L1
        scoreEvent = "SCORE_EVT";
        scoreAttribute = "SCORE_ATR";
        chLvlAttribute = "CHP_LVL";
        
        
    }

    public void ChangeSocialNames(string lbSocialName)
    {
        lbSocialN = lbSocialName;

        if(lbSocialN == "")
        {
            scoreSocial = "";
        }
    }

    public void PostScoreBttn()
    {
        
        //lvl = player.GetComponent<PlayerMovement>().currentLvl;
        highScStr = "HighScore" + chLvlValue;

        highSc = PlayerPrefs.GetFloat(highScStr, 0);

        time = "Time" + chLvlValue;
        timeValue = PlayerPrefs.GetString(time, "0");
        
        Debug.Log("Posting Score To Leaderboard...");
        Debug.Log(highSc + ": " + highScStr);
        new GameSparks.Api.Requests.LogEventRequest()
            .SetEventKey(scoreEvent)
            .SetEventAttribute(scoreAttribute, (long)highSc)
            .SetEventAttribute(chLvlAttribute, chLvlValue)
            .SetEventAttribute("TIME_F", timeValue)
            .Send((response) => {

                if (!response.HasErrors)
                {
                    Debug.Log("Score Posted Sucessfully " + scoreEvent + " : " + scoreAttribute + " : " + chLvlAttribute + " : " + chLvlValue + " : " + highSc.ToString());
                    Debug.Log("time value: " + timeValue);
                }
                else
                {
                    Debug.Log("Error Posting Score...");
                }
            });
    }

    public void GetLeaderboard()
    {
        Debug.Log("Fetching Leaderboard Data...");

        if(Application.internetReachability == NetworkReachability.NotReachable)
        {

            rankTxt.text = System.String.Empty;
            nameTxt.text = System.String.Empty;
            scoreTxt.text = System.String.Empty;
            timeTxt.text = System.String.Empty;
            outputData.gameObject.SetActive(true);
            outputData.text = "You're not connected to the Internet. Please try again!";
        }
        else
        {
            new GameSparks.Api.Requests.LeaderboardDataRequest()
            .SetLeaderboardShortCode(lbName)
            .SetEntryCount(10) // we need to parse this text input, since the entry count only takes long
            .Send((response) => {

                if (!response.HasErrors)
                {
                    Debug.Log("Found Leaderboard Data...");
                    outputData.text = System.String.Empty; // first clear all the data from the output

                    outputData.gameObject.SetActive(false);

                    rankTxt.text = System.String.Empty;
                    nameTxt.text = System.String.Empty;
                    scoreTxt.text = System.String.Empty;
                    timeTxt.text = System.String.Empty;
                    foreach (GameSparks.Api.Responses.LeaderboardDataResponse._LeaderboardData entry in response.Data) // iterate through the leaderboard data
                    {
                        int rank = (int)entry.Rank; // we can get the rank directly
                        string playerName = entry.UserName;
                        string score = entry.JSONData[scoreAttribute].ToString(); // we need to get the key, in order to get the score
                        //outputData.text += rank + "\t\t\t" + playerName + "\t\t\t" + score + "\n"; // addd the score to the output text
                        string timeT = entry.JSONData["TIME_F"].ToString();

                        rankTxt.text += rank + "\n";
                        nameTxt.text += playerName + "\n";
                        scoreTxt.text += score + "\n";
                        timeTxt.text += timeT + "\n";
                    }

                    up.ButtonEnterUpdateSpacing();
                }
                else
                {
                    outputData.gameObject.SetActive(true);

                    rankTxt.text = System.String.Empty;
                    nameTxt.text = System.String.Empty;
                    scoreTxt.text = System.String.Empty;
                    timeTxt.text = System.String.Empty;
                    //Debug.Log("Error Retrieving Leaderboard Data...");
                    outputData.text = "Error Retrieving Leaderboard Data. Please try again!";
                }

            });
            Debug.Log(lbName);

        }
        up.BackUpdateSpacing();
    }
	
	public void GetSocialLeaderboard(){

        Debug.Log("Fetching social leaderboard data");

        if(Application.internetReachability == NetworkReachability.NotReachable)
        {

            rankTxt.text = System.String.Empty;
            nameTxt.text = System.String.Empty;
            scoreTxt.text = System.String.Empty;
            timeTxt.text = System.String.Empty;
            outputData.gameObject.SetActive(true);
            outputData.text = "You're not connected to the Internet. Please try again!";
        }
        else
        {
            new GameSparks.Api.Requests.SocialLeaderboardDataRequest()
            .SetLeaderboardShortCode(lbName)
            .SetEntryCount(10) // we need to parse this text input, since the entry count only takes long
            .Send((response) => {

                if (!response.HasErrors)
                {
                    Debug.Log("Found Leaderboard Data...");
                    outputData.text = System.String.Empty; // first clear all the data from the output
                    outputData.gameObject.SetActive(false);
                    rankTxt.text = System.String.Empty;
                    nameTxt.text = System.String.Empty;
                    scoreTxt.text = System.String.Empty;
                    timeTxt.text = System.String.Empty;
                    foreach (GameSparks.Api.Responses.LeaderboardDataResponse._LeaderboardData entry in response.Data) // iterate through the leaderboard data
                    {
                        int rank = (int)entry.Rank; // we can get the rank directly
                        string playerName = entry.UserName;
                        string score = entry.JSONData[scoreAttribute].ToString(); // we need to get the key, in order to get the score
                        //outputData.text += rank + "\t\t\t" + playerName + "\t\t\t" + score + "\n"; // addd the score to the output text
                        string timeT = entry.JSONData["TIME_F"].ToString();

                        rankTxt.text += rank + "\n";
                        nameTxt.text += playerName + "\n";
                        scoreTxt.text += score + "\n";
                        timeTxt.text += timeT + "\n";
                        
                    }
                    up.ButtonEnterUpdateSpacing();
                }
                else
                {
                    outputData.gameObject.SetActive(true);

                    rankTxt.text = System.String.Empty;
                    nameTxt.text = System.String.Empty;
                    scoreTxt.text = System.String.Empty;
                    timeTxt.text = System.String.Empty;
                    //Debug.Log("Social LB: Error Retrieving Leaderboard Data...");
                    outputData.text = "Error Retrieving Leaderboard Data. Please try again!";
                }

            });
        }

        up.BackUpdateSpacing();
    }


    public void DisplayCurrentPlayerLeaderboard()
    {
        currentPlayerLB.text = System.String.Empty;
        List<string> leaderboardCodes = new List<string>();
        leaderboardCodes.Add(lbName);

        new GameSparks.Api.Requests.AccountDetailsRequest()
            .Send((response) =>
            {
                if (!response.HasErrors)
                {
                    string PlayerID = response.UserId;
                    
                    new GameSparks.Api.Requests.GetLeaderboardEntriesRequest().SetPlayer(PlayerID).SetLeaderboards(leaderboardCodes).Send((response2) =>
                    {
                        string name = response2.BaseData.GetGSData(lbName).GetString("userName");
                        int score = (int)response2.BaseData.GetGSData(lbName).GetNumber(scoreAttribute);
                        int rank = (int)response2.BaseData.GetGSData(lbName).GetNumber("rank");
                        string timeT = response2.BaseData.GetGSData(lbName).GetString("TIME_F");

                        currentPlayerLB.text = rank + "\t\t" +name + "\t\t"+score + "\t\t" + timeT;

                        Debug.Log(rank.ToString());
                    });
                }
                else
                {
                    currentPlayerLB.text = "";
                }
            });
        
    }
}
