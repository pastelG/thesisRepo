﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowLevelInfo : MonoBehaviour {

    private int indexLoad;
    public Text highS, lvlTxt;
    private float score;
    private string highStr;
    public GameObject thisPanel, comicChapter1, imgChp1, btnSkip;
    public GameObject gsLead;
    public ComicScript com;
    
    public int chpNum = 1;
    private string clCode;
    

    private void Awake()
    {
        
    }

    // Use this for initialization
    void Start () {

        
        

        
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    //Current Level Number
    public void btnClick2(string lvl)
    {
        lvlTxt.text = lvl;

      
        highStr = "HighScore" + clCode;
        Debug.Log("chapter 1 " + highStr);

    }

    //Index Load Scene
    public void btnClickLvl(int idxLoad)
    {
        //this.dl = dreamloLeaderBoard.GetSceneDreamloLeaderboard();

        indexLoad = idxLoad;
        thisPanel.gameObject.SetActive(true);
        //highStr = "HighScore" + lvlTxt.text;
        score = PlayerPrefs.GetFloat(highStr, 0);
        highS.text = "High Score: " + ((int)score).ToString();

        //dl.AddScore(PlayerPrefs.GetString("user", ""), (int)score);
        Debug.Log(highS.text);
        Debug.Log(highStr);
    }

    public void levelStart()
    {
        //if it is chapter 1 and level 1, load scene
        if (chpNum == 1 && lvlTxt.text == "1")
        {
            Time.timeScale = 1;
            comicChapter1.SetActive(true);
            com.playChp();
            if (PlayerPrefs.GetInt("ReachLevel", 1) > 1)
                btnSkip.SetActive(true);
            Debug.Log("playing animation");
        }
        else
            FindObjectOfType<LoadSceneOnClick>().LoadByIndex(indexLoad);
    }

    //wait for the animation to end, then the animation event will call this function
    public void levelChp1AnimationStart()
    {
        
        FindObjectOfType<LoadSceneOnClick>().LoadByIndex(3);
        
    }

    public void chapterNum(int chp)
    {
        chpNum = chp;

        
    }

    public void ChapterLevelCode(string code)
    {
        clCode = code;

        gsLead.GetComponent<GS_Leaderboard>().ChangeChLvlValuePostScore(clCode);
    }
}
