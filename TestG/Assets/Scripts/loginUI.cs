﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Core;
using Facebook.Unity;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class loginUI : MonoBehaviour
{
    private static loginUI loginInstance;


    public GameObject btnLogin, btnFBLO, imgFBprof, pnlPrevDev, canvas;
    public Text extra;
    //public GameObject imgFBprof2, txtUser;
    public coinsGamesparks coinScr;

    private int scI = 0;

    private string userId;

    void Awake()
    {
        /*if (loginInstance == null)
        {
            loginInstance = this;
            

        }
        else
        {
            
        }*/

        //btnLogin = canvas.transform.Find("btnLogin").gameObject;
        btnFBLO = GameObject.Find("btnLogout");

        imgFBprof = canvas.transform.Find("fbDP").gameObject;

        //pnlPrevDev = canvas.transform.Find("pnlAlreadyDeviceLogged").gameObject;
        //pnlPrevDev = GameObject.Find("pnlAlreadyDeviceLogged");

        //pnlPrevDev.SetActive(false);

        extra = GameObject.Find("Extra").GetComponent<Text>();
        coinScr = GameObject.Find("GameSparksObj").GetComponent<coinsGamesparks>();



        Debug.Log("Scene: " + scI);
        scI = SceneManager.GetActiveScene().buildIndex;

        Debug.Log("Awake");
        if (!FB.IsInitialized)
        {
            // Initialize the Facebook SDK
            FB.Init(SetInit, OnHideUnity);


        }
        else
        {
            // Already initialized, signal an app activation App Event
            FB.ActivateApp();
            DealWithFBMenus(FB.IsLoggedIn);

        }
        /*
        if (FB.IsLoggedIn)
        {
            btnFBLO.SetActive(true);
            imgFBprof.SetActive(true);
            FB.API("/me/picture?type=square&height=128&width=128", HttpMethod.GET, DisplayProfilePic);
        }*/
    }

    void Start()
    {
        Debug.Log("Start");

        

    }

    void SetInit()
    {
        bool h = bool.Parse("true");

        FB.ActivateApp();
        if (FB.IsLoggedIn)
        {
            Debug.Log("FB is logged in");

        }
        else
        {
            Debug.Log("FB is not logged in");

        }

        DealWithFBMenus(bool.Parse(PlayerPrefs.GetString("fbLogged", "false")));

    }

    void OnHideUnity(bool isGameShown)
    {

        if (!isGameShown)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }

    }

    private void Update()
    {



    }

    void DealWithFBMenus(bool isLoggedIn)
    {

        if (isLoggedIn)
        {
            FB.Mobile.RefreshCurrentAccessToken();
            btnFBLO.SetActive(true);
            imgFBprof.SetActive(true);
            FB.API("/me/picture?type=square&height=128&width=128", HttpMethod.GET, DisplayProfilePic);
        }
        else
        {
            //btnFBLO.SetActive(false);
            //imgFBprof.SetActive(false);
            //btnLogin.SetActive(true);
            extra.text = "Welcome!";
        }

    }

    public void DeviceAuthentication_bttn()
    {
       
        Debug.Log("Attempting Device Authentication...");
        new GameSparks.Api.Requests.DeviceAuthenticationRequest()
            .SetDisplayName(PlayerPrefs.GetString("user", ""))
            .Send((auth_response) =>
            {
                if (!auth_response.HasErrors)
                { // for the next part, check to see if we have any errors i.e. Authentication failed
                    //connectionInfoField.text = "GameSparks Authenticated...";
                    //userNameField.text = auth_response.DisplayName;
                    //btnLogin.SetActive(false);
                    bool? newplayer = auth_response.NewPlayer;
                    Debug.Log(newplayer);

                    imgFBprof.SetActive(true);
                    userId = auth_response.UserId;
                    PlayerPrefs.SetString("userId",userId);
                    if(newplayer == false)
                    {
                        pnlPrevDev.SetActive(true);
                    }
                    extra.text = "Welcome, " + auth_response.DisplayName + "!";
                    PlayerPrefs.SetString("user", auth_response.DisplayName);

                }
                else
                {
                    Debug.LogWarning("device auth error: " + auth_response.Errors.JSON); // if we have errors, print them out
                }
            });
        coinScr.LoadCoinsFromGameSparks();
    }


    public void FacebookConnect_bttn()
    {

        Debug.Log("Connecting Facebook With GameSparks...");// first check if FB is ready, and then login //
                                                            // if it's not ready we just init FB and use the login method as the callback for the init method //
        if (!FB.IsInitialized)
        {
            Debug.Log("Initializing Facebook...");
            FB.Init(ConnectGameSparksToGameSparks, null);
        }
        else
        {
            FB.ActivateApp();
            ConnectGameSparksToGameSparks();
        }
        coinScr.LoadCoinsFromGameSparks();
    }

    ///<summary>
    ///This method is used as the delegate for FB initialization. It logs in FB
    /// </summary>
    private void ConnectGameSparksToGameSparks()
    {
        if (FB.IsInitialized)
        {
            FB.ActivateApp();
            Debug.Log("Logging Into Facebook...");
            var perms = new List<string>() { "public_profile", "email", "user_friends" };
            FB.LogInWithReadPermissions(perms, (result) =>
            {
                if (FB.IsLoggedIn)
                {
                    FB.Mobile.RefreshCurrentAccessToken();
                    PlayerPrefs.SetString("fbLogged", "true");
                    Debug.Log("Logged In, Connecting GS via FB..");
                    new GameSparks.Api.Requests.FacebookConnectRequest()
                     .SetAccessToken(AccessToken.CurrentAccessToken.TokenString)
                     .SetDoNotLinkToCurrentPlayer(true)// we don't want to create a new account so link to the player that is currently logged in
                     .SetSwitchIfPossible(true)//this will switch to the player with this FB account id they already have an account from a separate login

                     .Send((fbauth_response) =>
                     {
                         if (!fbauth_response.HasErrors)
                         {
                             //connectionInfoField.text = "GameSparks Authenticated With Facebook...";
                             //userNameField.text = fbauth_response.DisplayName;
                             userId = fbauth_response.UserId;
                             PlayerPrefs.SetString("userId", userId);
                             PlayerPrefs.SetString("user", fbauth_response.DisplayName);

                             extra.text = "Welcome, " + fbauth_response.DisplayName + "!";
                         }
                         else
                         {
                             Debug.LogWarning(fbauth_response.Errors.JSON);//if we have errors, print them out
                         }
                     });
                    btnLogin.SetActive(false);
                    imgFBprof.SetActive(true);
                    FB.API("/me/picture?type=square&height=128&width=128", HttpMethod.GET, DisplayProfilePic);
                }
                else
                {
                    Debug.LogWarning("Facebook Login Failed:" + result.Error);
                    DeviceAuthentication_bttn();
                }
            });// lastly call another method to login, and when logged in we have a callback
        }
        else
        {
            FacebookConnect_bttn();// if we are still not connected, then try to process again
        }
    }


    void DisplayProfilePic(IGraphResult result)
    {

        if (result.Texture != null)
        {

            Image ProfilePic = imgFBprof.GetComponent<Image>();
            ProfilePic.sprite = Sprite.Create(result.Texture, new Rect(0, 0, 128, 128), new Vector2());

        }

    }



    public void DeviceLogOut()
    {
        btnLogin.SetActive(true);
        PlayerPrefs.DeleteKey("user");
    }

    public void FBlogout()
    {
        
        FB.LogOut();
        btnLogin.SetActive(true);
        imgFBprof.SetActive(false);
        PlayerPrefs.DeleteAll();
        PlayerPrefs.DeleteKey("user");
        PlayerPrefs.SetString("fbLogged", "false");
        extra.text = "Welcome!";
    }

}
