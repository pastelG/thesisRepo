﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour {

    [SerializeField]
    private HealthStat health;

    //private EnemyScript enemyScript;

    public CharacterController2D controller;
    public Animator animator;

    //public Button leftBtn, rightBtn;
    public GameObject canvasQuiz, choicesQuiz,tfQuiz, idenQuiz, hintPnl, btnL, btnR, glowI, txtCurrentScore, txtScore, highScore, stopWatch, audioS, playerWinCanvas, canvasEnding, pauseInQ;

    public float runSpeed = 2f;

    float horizontalMove = 0f;

    public bool EnemyMove = false;
    public bool playWin = false;
    public float currentSc;

    public bool endGame = false;
    private GameObject enemy;

    public int currentLvl;
    private int reachLvl;
    private int reach2Lvl;
    private int reach3Lvl;
    private float highSc;

    private string highScStr;
    string playername = "";

    public bool isPerfect;

    private float secs;
    private float totalScore;

    private string clCode;

    private ColorBlock colorsV;

    private GameObject[] gameObjs;
    

    private void Awake()
    {
        health.Initialize();

        //enemyScript = GameObject.FindObjectOfType<EnemyScript>();
        //questionManager = GameObject.FindObjectOfType<QuestionManager>();
        reachLvl = PlayerPrefs.GetInt("ReachLevel", 1);
        reach2Lvl = PlayerPrefs.GetInt("Reach2Level", 0);
        reach3Lvl = PlayerPrefs.GetInt("Reach3Level", 0);

        

        playername = PlayerPrefs.GetString("user", "");

        stopWatch = GameObject.Find("txtStopWatch");

        clCode = playerWinCanvas.GetComponent<PlayerWinGamesparks>().chapterLevelCode;

        highScStr = "HighScore" + clCode;

        highSc = PlayerPrefs.GetFloat(highScStr, 0);

        
    }

    // Use this for initialization
    void Start()
    {
        

        btnL = GameObject.Find("leftButton");
        btnR = GameObject.Find("rightButton");

        
    }
	
	// Update is called once per frame
	void Update () {
        horizontalMove = CrossPlatformInputManager.GetAxisRaw("Horizontal") *runSpeed;
        animator.SetFloat("Speed", Mathf.Abs(horizontalMove));

        if (endGame == true)
        {
            Time.timeScale = 0;
            /*for (int i = 0; i < 4; i++)
            {
                glowI.GetComponent<Image>().canvasRenderer.SetAlpha(1.0f);
                StartP();
                glowI.GetComponent<Image>().CrossFadeAlpha(0.5f, 1.5f, false);
            }*/
            
        }
	}

    IEnumerator StartP()
    {
        yield return new WaitForSeconds(2.5f);
    }

    void FixedUpdate()
    {
        //Move our character
        controller.Move(horizontalMove * Time.fixedDeltaTime, false, false);
        
    }
    
    

    public void OnClickDestroyEnemy(bool playerWin)
    {
        playWin = playerWin;
        
        if (enemy.gameObject.tag == "EnemyTF")
            tfQuiz.gameObject.SetActive(false);
        else if (enemy.gameObject.tag == "Enemy")
            choicesQuiz.gameObject.SetActive(false);
        else if (enemy.gameObject.tag == "EnemyI")
            idenQuiz.gameObject.SetActive(false);

        if(enemy.gameObject.tag == "EnemyTF" || enemy.gameObject.tag == "Enemy" || enemy.gameObject.tag == "EnemyI")
        {
            colorsV = pauseInQ.GetComponent<Button>().colors;
            colorsV.normalColor = Color.black;
            pauseInQ.GetComponent<Button>().colors = colorsV;
            pauseInQ.GetComponentInChildren<Text>().color = Color.white;
        }

        hintPnl.gameObject.SetActive(false);

        if (playerWin == true)
        {
            Debug.Log("this paramater is true");

            enemy.GetComponent<EnemyScript>().EnemyDead();
           
            DestroyEnemy();
            animator.SetBool("PlyWins", true);
            animator.SetBool("ColEnm", false);
        }
        else
        {
            Debug.Log("This paramater is false");
            enemy.GetComponent<CircleCollider2D>().enabled = false;
            EnemyMove = true;

            enemy.GetComponent<EnemyScript>().EnemyMovement(EnemyMove);
            

            Destroy(enemy, 3);
            animator.SetBool("PlyWins", false);
            animator.SetBool("ColEnm", false);

            health.CurrentVal -= 1;
            
            
        }
        
        currentSc = health.CurrentVal;

        GameObject.Find("FightingAudio").GetComponent<AudioSource>().Stop();

        if (health.CurrentVal == 0)
        {
            GameObject.Find("errorAudio").GetComponent<AudioSource>().Play();
            FindObjectOfType<LoadSceneOnClick>().LoadByIndex(2);
        }
        audioS.gameObject.GetComponent<AudioSource>().UnPause();
        btnL.gameObject.SetActive(true);
        btnR.gameObject.SetActive(true);
       
    }
    public void DestroyEnemy()
    {
        
        if (playWin == true)
        {
            
            Debug.Log("Player Wins");
            Destroy(enemy, 2);
        }


        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.gameObject.tag == "EnemyTF" || collision.gameObject.tag == "Enemy" || collision.gameObject.tag == "EnemyI")
        {
            colorsV = pauseInQ.GetComponent<Button>().colors;
            colorsV.normalColor = Color.white;
            pauseInQ.GetComponent<Button>().colors = colorsV;
            pauseInQ.GetComponentInChildren<Text>().color = Color.black;
        }

        if (collision.gameObject.tag == "Enemy")
        {
            audioS.gameObject.GetComponent<AudioSource>().Pause();

            GameObject.Find("FightingAudio").GetComponent<AudioSource>().Play();
            btnL.gameObject.SetActive(false);
            btnR.gameObject.SetActive(false);
            Debug.Log("Collided with an Enemy");
            animator.SetBool("ColEnm",true);
            //canvasQuiz.gameObject.SetActive(true);
            choicesQuiz.gameObject.SetActive(true);
            hintPnl.gameObject.SetActive(true);

            enemy = collision.gameObject;
            
            //StartP();
            enemy.GetComponent<CircleCollider2D>().isTrigger = true;
            enemy.GetComponent<EnemyScript>().EnemyAttack();
            
        }
        else if(collision.gameObject.tag == "EnemyTF")
        {
            audioS.gameObject.GetComponent<AudioSource>().Pause();

            GameObject.Find("FightingAudio").GetComponent<AudioSource>().Play();
            btnL.gameObject.SetActive(false);
            btnR.gameObject.SetActive(false);
            animator.SetBool("ColEnm", true);
            
            tfQuiz.gameObject.SetActive(true);
            hintPnl.gameObject.SetActive(true);

            enemy = collision.gameObject;

            enemy.GetComponent<CircleCollider2D>().isTrigger = true;
            enemy.GetComponent<EnemyScript>().EnemyAttack();
        }
        else if (collision.gameObject.tag == "EnemyI")
        {
            audioS.gameObject.GetComponent<AudioSource>().Pause();

            GameObject.Find("FightingAudio").GetComponent<AudioSource>().Play();
            btnL.gameObject.SetActive(false);
            btnR.gameObject.SetActive(false);
            animator.SetBool("ColEnm", true);
            
            idenQuiz.gameObject.SetActive(true);
            hintPnl.gameObject.SetActive(true);

            enemy = collision.gameObject;

            enemy.GetComponent<CircleCollider2D>().isTrigger = true;
            enemy.GetComponent<EnemyScript>().EnemyAttack();
        }
        else
        {
            animator.SetBool("ColEnm", false);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Finish")
        {
            if(health.MaxVal == health.CurrentVal)
            {
                isPerfect = true;
            }

            

            secs = stopWatch.GetComponent<StopWatch>().timer;
            Debug.Log("Timer Seconds " + secs);

            btnL.gameObject.SetActive(false);
            btnR.gameObject.SetActive(false);
            Debug.Log("Finish Line");
            playerWinCanvas.gameObject.SetActive(true);
            audioS.gameObject.GetComponent<AudioSource>().Stop();
            
            //endGame = true;

            

            if(secs != 0)
            {
                totalScore = (currentSc + (currentSc / (secs / 100))) * 10;
            }

            

            Debug.Log("Current Score " + currentSc);
            Debug.Log("Total Score " + totalScore);

            if (highSc < totalScore)
            {
                highSc = totalScore;
                PlayerPrefs.SetFloat(highScStr, highSc);
                
            }

            //displays player data if he/she wins
            txtCurrentScore.GetComponent<Text>().text = ((int)currentSc).ToString();
            txtScore.GetComponent<Text>().text = ((int)totalScore).ToString();
            highScore.GetComponent<Text>().text = "High Score: " + ((int)highSc).ToString();

            
            if (clCode == "C1L1" || clCode == "C1L2")
            {
                if (currentLvl == reachLvl)
                {
                    PlayerPrefs.SetInt("ReachLevel", currentLvl + 1);
                }
            }
            else if(clCode == "C1L3")
            {
                PlayerPrefs.SetInt("Reach2Level", 1);
            }
            else if (clCode == "C2L1" || clCode == "C2L2")
            {
                if(currentLvl == reach2Lvl)
                {
                    PlayerPrefs.SetInt("Reach2Level", currentLvl + 1);
                }
            }
            else if (clCode == "C2L3")
            {
                PlayerPrefs.SetInt("Reach3Level", 1);
            }
            else if (clCode == "C3L1" || clCode == "C3L2")
            {
                if (currentLvl == reach3Lvl)
                {
                    PlayerPrefs.SetInt("Reach3Level", currentLvl + 1);
                }
            }
            else if (clCode == "C3L3")
            {
                PlayerPrefs.SetInt("Reach4Level", 1);

                canvasEnding.SetActive(true);

                GameObject.Find("WinAudio").GetComponent<AudioSource>().Stop();
            }

            if(clCode != "C3L3")
            {
                endGame = true;
                GameObject.Find("WinAudio").GetComponent<AudioSource>().Play();
            }
        }
    }


}
