﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Api.Requests;
using GameSparks.Core;

public class coinsGamesparks : MonoBehaviour
{
    public void SaveCoinsToGameSparks(int amount)
    {
        new LogEventRequest().SetEventKey("SAVE_PLAYER").SetEventAttribute("COINS", amount).Send((response) =>
        {
            if (response.HasErrors)
            {
                Debug.Log("Save Coins Log message failed ...");
            }
            else
            {
                Debug.Log("Save Coins Log message succeeded ..." + response);
            }
        });
    }

    public void LoadCoinsFromGameSparks()
    {
        new LogEventRequest().SetEventKey("LOAD_PLAYER").Send((response) => {
            if (!response.HasErrors)
            {
                Debug.Log("Received Player Data From GameSparks...");
                GSData data = response.ScriptData.GetGSData("player_Data");
                print("Player coins: " + data.GetInt("coins"));
                PlayerPrefs.SetInt("coins", (int)(data.GetInt("coins")));

            }
            else
            {
                Debug.Log("Error Loading Player Data...");
            }
        });
    }
}
