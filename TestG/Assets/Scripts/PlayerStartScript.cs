﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStartScript : MonoBehaviour {

    string user, firstT, lastT;
    public GameObject panel, firstN, lastN, welT, btnLogin;
    public GameObject txtExtra;


    private GameObject gamesparksObj;

    private void Awake()
    {
        user = PlayerPrefs.GetString("user", "");
        //panel = GameObject.Find("FullNamePnl");
        if (user == "" || user == null)
        {
            Debug.Log("user is null");
            panel.gameObject.SetActive(true);
            welT.GetComponent<Text>().text = "Welcome!";
        }
        else
        {
            Debug.Log(user);
            welT.GetComponent<Text>().text = "Welcome, " + user + "!";
            btnLogin.SetActive(false);
        }

        gamesparksObj = GameObject.Find("GameSparksObj");
    }

    // Use this for initialization
    void Start () {
        user = PlayerPrefs.GetString("user", "");
        //panel = GameObject.Find("FullNamePnl");
        if (user == "" || user == null)
        {
            Debug.Log("user is null");
            panel.gameObject.SetActive(true);
            welT.GetComponent<Text>().text = "Welcome!";
        }
        else
        {
            Debug.Log(user);
            welT.GetComponent<Text>().text = "Welcome, " + user + "!";
            btnLogin.SetActive(false);
        }

        gamesparksObj = GameObject.Find("GameObjectLoginUI");
    }
	
	// Update is called once per frame
	void Update () {
        
    }

    public void SendFullName(string loginMethod)
    {
        firstT = firstN.GetComponent<InputField>().text;
        lastT = lastN.GetComponent<InputField>().text;

        user = firstT + " " + lastT;

        PlayerPrefs.SetString("user", user);
        //panel.gameObject.SetActive(false);

        RequiredFields(firstT, lastT, loginMethod);

        Debug.Log(user);
        
    }

    public void RequiredFields(string first, string last, string loginMethod)
    {
        if (loginMethod == "device")
        {
            if (first == null || last == null || first == "" || last == "")
            {
                txtExtra.SetActive(true);
            }
            else
            {
                txtExtra.SetActive(false);
                gamesparksObj.GetComponent<loginUI>().DeviceAuthentication_bttn();
                PlayerPrefs.SetString("user", first+" "+last);

                panel.gameObject.SetActive(false);

                welT.GetComponent<Text>().text = "Welcome, " + user + "!";
                btnLogin.SetActive(false);
            }
        }
        else if(loginMethod == "facebook")
        {
            gamesparksObj.GetComponent<loginUI>().FacebookConnect_bttn();

            panel.gameObject.SetActive(false);

            
        }



    }
}
