﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerWinGamesparks : MonoBehaviour
{
    public string chapterLevelCode;

    public GameObject gamesparkM, rewardImg, stopW, playerWin;
    public Text victoryTxt, timeFinishedTxt;
   

    private coinsGamesparks coinScr;
    private PlayerMovement playM;
    private bool isPerfect;
    private float currScore;

    private int score;
    

    // Start is called before the first frame update
    void Start()
    {

        
        
        if(this.enabled == true)
        {
            //chapterLevelCode = playerWin.GetComponent<PlayerWinGamesparks>().chapterLevelCode;

            score = PlayerPrefs.GetInt("AnsQ" + chapterLevelCode, 0);
        

            

            coinScr = GameObject.Find("GameSparksObj").GetComponent<coinsGamesparks>();
            playM = GameObject.Find("player").GetComponent<PlayerMovement>();

            isPerfect = playM.isPerfect;

            currScore = playM.currentSc;


            timeFinishedTxt.text = stopW.GetComponent<StopWatch>().timerString;

            
            //if current score is higher than the previous score that has been stored in PlayerPrefs
            //in short, naghighscore si player
            if (score < currScore)
            {
                //GameObject.Find("GameSparksObj").GetComponent<ChapterLvlGamesparks>().ChapterLevelCode(chapterLevelCode);
                GameObject.Find("GameSparksObj").GetComponent<ChapterLvlGamesparks>().SaveScoreToGameSparks((int)currScore);
                PlayerPrefs.SetInt("AnsQ" + chapterLevelCode, (int)currScore);
                PlayerPrefs.SetString("Time" + chapterLevelCode, timeFinishedTxt.text);
            }

            gamesparkM.GetComponent<GS_Leaderboard>().PostScoreBttn();

            if (isPerfect == true)
            {
                //shows an ui text that the player have completed with a perfect score
                //then rewards the player coins 

                victoryTxt.text = "Perfect Score!";

                //set the ui that rewards the player active
                rewardImg.gameObject.SetActive(true);
            }

            
            
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnClickReward(int rewardCoins)
    {
        int coins = PlayerPrefs.GetInt("coins", 10);
        coinScr.SaveCoinsToGameSparks(coins + rewardCoins);
        PlayerPrefs.SetInt("coins", coins + rewardCoins);
    }
}
