﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameSparks.Api.Requests;
using GameSparks.Core;

public class ChapterLvlGamesparks : MonoBehaviour
{
    private string shortCode;



    public void ChapterLevelCode(string code)
    {
        shortCode = code;
    }
    
    public void SaveScoreToGameSparks(int score)
    {
        shortCode = GameObject.Find("PlayerWinCanvas").GetComponent<PlayerWinGamesparks>().chapterLevelCode;

        new LogEventRequest().SetEventKey("ANS_"+shortCode).SetEventAttribute(shortCode, score).Send((response) =>
        {
            if (response.HasErrors)
            {
                Debug.Log("Save Chapter Level short code Log message failed ...");
            }
            else
            {
                Debug.Log(shortCode + " " + score + " Save Chapter Level short code Log message succeeded ..." + response);
            }
        });
    }

    public void LoadChapterLevelCodeFromGameSparks()
    {
        new LogEventRequest().SetEventKey("LOAD_PLAYER").Send((response) => {
            if (!response.HasErrors)
            {
                Debug.Log("Received Player Data From GameSparks...");

                GSData dataChp1 = response.ScriptData.GetGSData("Chapter1");

                print("Player ID: " + dataChp1.GetInt("level 1"));
                
            }
            else
            {
                Debug.Log("Error Loading Player Data...");
            }
        });
    }
}
