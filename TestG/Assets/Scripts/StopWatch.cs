﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Diagnostics;
using UnityEngine.UI;

using Debug=UnityEngine.Debug;

public class StopWatch : MonoBehaviour
{
    public Text txtStop;
    public Stopwatch sW = new Stopwatch();

    public int secondsInt;

    private int x;
    public float timer = 0.0f;
    public string timerString;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        /*
        if (Input.GetKeyDown("tab"))
        {
            sW.Start();
        }

        if (Input.GetKeyDown("space"))
        {
            TimeSpan ts = sW.Elapsed;
            sW.Stop();
            x = (int) ts.TotalSeconds;

            txtStop.text = x.ToString();

            sW.Reset();
        }
        */

        timer += Time.deltaTime;
        secondsInt = (int)timer % 60;
        int timeT = (int)timer;
        int minutes = (int)Mathf.Floor(timer / 60);

        timerString = minutes.ToString() + ":" + secondsInt.ToString();

        txtStop.text = "Time" + " <b>" + timerString + "</b>";

    }
}
