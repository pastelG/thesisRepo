﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComicEndingScript : MonoBehaviour
{
    public GameObject CanvasEnding, player;
    public Animation anim;

    // Start is called before the first frame update
    void Start()
    {
        anim.Play();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void disabledCanvasEnding()
    {
        CanvasEnding.SetActive(false);
        player.GetComponent<PlayerMovement>().endGame = true;
        GameObject.Find("WinAudio").GetComponent<AudioSource>().Play();
    }
}
