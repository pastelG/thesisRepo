﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestionTrigger : MonoBehaviour {

    public GameObject questionManager, choicesQuiz, tfQuiz, iQuiz, errorHint;
    public Question[] questions;
    public QuestionPart2[] questions2, questions3;
    
    

    // Use this for initialization
    void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (choicesQuiz.activeInHierarchy == true)
        {
            
            FindObjectOfType<QuestionManager>().StartDisplayChoicesQuestion(questions);
        }
        else if (tfQuiz.activeInHierarchy == true)
        {
            FindObjectOfType<QuestionManager>().StartDisplayTFQuestion(questions2);
        }
        else if(iQuiz.activeInHierarchy == true)
        {
            FindObjectOfType<QuestionManager>().StartDisplayIdenQuestion(questions3);
        }
        
    }

    public void AnswerRight(Text clickedT)
    {
        FindObjectOfType<QuestionManager>().CheckIfRightChoice(questions, clickedT);

        
    }

    public void CheckTrueFalse(Text clickedT)
    {
        
        if (tfQuiz.activeInHierarchy == true)
        {
            FindObjectOfType<QuestionManager>().OnClickTrueFalse(questions2, clickedT);
        }
        else if (iQuiz.activeInHierarchy == true)
        {
            FindObjectOfType<QuestionManager>().OnClickTrueFalse(questions3, clickedT);
        }
    }

    public void OnClickHintBttn()
    {
        FindObjectOfType<QuestionManager>().CheckHint(questions, questions2, questions3);
    }

    /*public void OnClickDisplayHint()
    {
        FindObjectOfType<QuestionManager>().DisplayHint(questions, questions2, questions3);
    }*/

    public void OnClickUnlockHint()
    {
        if (PlayerPrefs.GetInt("coins", 10) > 0)
            FindObjectOfType<QuestionManager>().UnlockHint(questions, questions2, questions3);
        else
            errorHint.SetActive(true);
    }
}
