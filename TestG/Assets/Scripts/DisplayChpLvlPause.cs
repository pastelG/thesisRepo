﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayChpLvlPause : MonoBehaviour
{

    public Text txtCurrentLvlPause;
    private GS_Leaderboard gsLead;

    string chpLvl;
    private void Awake()
    {
        gsLead = GameObject.Find("GameSparkManager").GetComponent<GS_Leaderboard>();

        chpLvl = gsLead.chLvlValue;


        //trim C
        chpLvl = chpLvl.Trim('C');
        //replace L
        txtCurrentLvlPause.text = chpLvl.Replace('L', '-');
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
